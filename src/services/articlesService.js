let articles = [
  {
    id: 0,
    title: "How to cook turkey on natural gass grills",
    text:
      "First, this handout will be most effective if you use it as a tool. Every time you read this handout, read it along side another piece of writing (a journal article, a magazine, a web page, a novel, a text book, etc.). Locate a few nouns in the reading, and use the handout to analyze the article usage. If you practice a little bit at a time, this kind of analysis can help you develop a natural sensitivity to this complex system."
  },
  {
    id: 1,
    title: "Gas ovens",
    text:
      "Second, using articles correctly is a skill that develops over time through lots of reading, writing, speaking and listening. Think about the rules in this handout, but also try to pay attention to how articles are being used in the language around you. Simply paying attention can also help you develop a natural sensitivity to this complex system."
  },
  {
    id: 2,
    title: "Trill friends and family with a make it yourself pizza party",
    text:
      "Finally, although using the wrong article may distract a reader’s attention, it usually does not prevent the reader from understanding your meaning. So be patient with yourself as you learn."
  },
  {
    id: 3,
    title: "Deep friend turkey practice",
    text:
      "And yet it is well known that volcanoes have the capacity to generate big waves. The mechanism as ever is the displacement of a large volume of water."
  },
  {
    id: 4,
    title: "Choosing A quality cookware set",
    text:
      "The comparison between Saturday's imagery and Sentinel pictures acquired before the tsunami are telling."
  }
];

export function getArticles() {
  const cachedArticles = localStorage.getItem("articles");
  articles = JSON.parse(cachedArticles) || articles;
  articles.sort((a, b) => b.id - a.id);
  return articles;
}

export function getArticle(id) {
  articles = getArticles() || articles;
  return articles.find(article => article.id === id);
}

export function addArticle(article) {
  articles = getArticles();
  const newArticle = { ...article, id: articles.length };
  articles = articles.concat(newArticle);
  localStorage.setItem("articles", JSON.stringify(articles));
}
