import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Articles from "./components/articles";
import Article from "./components/article";
import ArticleForm from "./components/articleForm";
import "./App.css";

class App extends Component {
  render() {
    return (
      <main className="container">
        <Switch>
          <Route path="/articles" component={Articles} />
          <Route path="/article/:id" component={Article} />
          <Route path="/new" component={ArticleForm} />
          <Redirect from="/" to="/articles" />
        </Switch>
      </main>
    );
  }
}

export default App;
