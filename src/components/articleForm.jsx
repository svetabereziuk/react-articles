import React, { Component } from "react";
import { addArticle, getArticles } from "../services/articlesService";
import LastArticles from "./lastArticles";

class ArticleForm extends Component {
  state = {
    articles: [],
    title: "",
    text: ""
  };

  componentDidMount() {
    this.updateArticles();
  }

  updateArticles() {
    this.setState({ articles: getArticles().slice(0, 3) });
  }

  doSubmit = e => {
    e.preventDefault();
    const title = this.state.title;
    const text = this.state.text;
    title && text && addArticle({ title, text });
    this.state.title = "";
    this.state.text = "";
    this.updateArticles();
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <h1>New Article</h1>
            <form onSubmit={this.doSubmit}>
              <div className="form-group">
                <input
                  value={this.state.title}
                  onChange={this.handleChange}
                  placeholder="Article Title"
                  id="title"
                  type="text"
                  name="title"
                  className="form-control form-control-lg"
                />
                <small id="titleHelp" className="form-text text-muted">
                  Give it a short name
                </small>
              </div>
              <div className="form-group">
                <textarea
                  placeholder="Article Text"
                  id="text"
                  className="form-control"
                  rows="5"
                  value={this.state.text}
                  name="text"
                  onChange={this.handleChange}
                />
                <small id="textHelp" className="form-text text-muted">
                  Type the best article you can write.
                </small>
              </div>
              <div className="col-lg-4 px-0">
                <button type="submit" className="btn">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
        <LastArticles articles={this.state.articles} />
      </React.Fragment>
    );
  }
}

export default ArticleForm;
