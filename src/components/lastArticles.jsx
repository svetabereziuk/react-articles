import React, { Component } from "react";
import { Link } from "react-router-dom";

class LastArticles extends Component {
  render() {
    return (
        this.props.articles.map(article => (
          <article className="row" key={article.id}>
            <p className="col-md-12 title border-bottom p-4">
              <Link to={`/article/${article.id}`}>{article.title}</Link>
            </p>
          </article>
        ))
    );
  }
}

export default LastArticles;
