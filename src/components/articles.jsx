import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getArticles } from "../services/articlesService";

class Articles extends Component {
  state = {
    articles: getArticles()
  };

  handleNew = () => {
    this.props.history.push('/new');
  }

  render() {
    return (
      <section>
        <div className="row border-bottom">
          <div className="col-sm-12 col-lg-3 main-name py-5">
            <h1>Articles</h1>
          </div>
          <div className="col-sm-12 col-lg-3 main-button py-5">
            <button type="button" className="btn" onClick={this.handleNew}>
              Add New
            </button>
          </div>
        </div>
        {this.state.articles.map(article => (
          <article className="row" key={article.id}>
            <p className="col-md-12 title border-bottom p-4">
              <Link to={`/article/${article.id}`}>{article.title}</Link>
            </p>
          </article>
        ))}
      </section>
    );
  }
}

export default Articles;
