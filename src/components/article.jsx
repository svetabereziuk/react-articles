import React, { Component } from "react";
import { getArticle } from "../services/articlesService";

class Article extends Component {
  state = {
    article: {
      title: "",
      text: ""
    }
  };

  componentDidMount() {
    const articleId = parseInt(this.props.match.params.id);
    this.setState({ article: getArticle(articleId) });
  }

  render() {
    return (
      <article>
        <h1>{this.state.article.title}</h1>
        <p>{this.state.article.text}</p>
      </article>
    );
  }
}

export default Article;
